cmake_minimum_required(VERSION 3.0)

project(libuv)

set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)

if (WIN32)
    add_compile_options(/Z7 /MP)
endif ()

add_subdirectory(source)
