**Nuget packaging for libuv Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target libuv)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:libuv,INTERFACE_INCLUDE_DIRECTORIES>")

# Dependencies
find_package(Threads)
target_link_libraries(target ${CMAKE_THREAD_LIBS_INIT})
if (WIN32)
    target_link_libraries(target ws2_32 iphlpapi psapi userenv)
endif ()
```