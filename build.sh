#!/usr/bin/env bash

# Usage:
# build <generator> <arch> <config> <OS>

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$0 <generator> <arch> <config> <OS>"
    exit 0
fi

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
GENERATOR="$1"
ARCH="$2"
CONFIG="$3"
OS="$4"

if [[ "${GENERATOR}" == "Visual Studio"* ]]; then
    if [[ "${ARCH}" == "x86" ]]; then
        CMAKE_ARGS="-A Win32"
    else
        CMAKE_ARGS="-A ${ARCH}"
    fi
    
    BUILD_DIR="${BASE_DIR}/${GENERATOR} ${ARCH}"
    BUILD_ARGS="/consoleloggerparameters:ForceConsoleColor /maxcpucount:`nproc`"

elif [[ "${GENERATOR}" == "Unix Makefiles" ]]; then
    BUILD_DIR="${BASE_DIR}/${OS} ${GENERATOR} ${ARCH}/${CONFIG}"
    BUILD_ARGS="-j `nproc` VERBOSE=1"
    
else
    echo "Unsupported generator"
    exit 1
fi

rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"

pushd "${BUILD_DIR}"

    cmake -G "${GENERATOR}" ${CMAKE_ARGS} -DCMAKE_BUILD_TYPE=${CONFIG} "${BASE_DIR}"
    cmake --build . --config ${CONFIG} -- ${BUILD_ARGS}

popd
